DESCRIPTION = "Profiling package grooup for OE-Core"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
PR = "r0"

inherit pkgconfig

PACKAGES = "\
    ${PN} \
    ${PN}-dbg \
    ${PN}-dev \
    "

PACKAGE_ARCH = "${MACHINE_ARCH}"

ALLOW_EMPTY_${PN} = "1"


# dany: oprofileui-server
#       lttng-viewer
#       latencytop
# removed due to their x11 dependency.
#
# dany:  blktrace
#        sysprof
# removed due to their x11 dependency.
RDEPENDS_${PN} = "\
    oprofile \
    ${LTTNG} \
    "

LTTNG ?= "\
    lttng-tools \
    lttng-modules \
    lttng-ust \
    "
LTTNG_armv6 ?= ""

#    comment out perf since it can not been built in linux-2.6.33.9 kernel
#    perf

#    Will fail without a debug flavoured kernel
#    kernel-module-oprofile"

# lttng-ust uses sched_getcpu() which is not there on uclibc
# for some of the architectures it can be patched to call the
# syscall directly but for x86_64 __NR_getcpu is a vsyscall
# which means we can not use syscall() to call it. So we ignore
# it for x86_64/uclibc

LTTNGUST = "lttng-ust"
LTTNGUST_libc-uclibc = ""

#    exmap-console
#    exmap-server

# At present we only build lttng-ust on
# qemux86/qemux86-64/qemuppc/qemuarm/emenlow/atom-pc since upstream liburcu
# (which is required by lttng-ust) may not build on other platforms, like
# MIPS.
RDEPENDS_${PN}_append_qemux86 = " valgrind lttng-ust"
RDEPENDS_${PN}_append_qemux86-64 = " ${LTTNGUST}"
RDEPENDS_${PN}_append_qemuppc = " ${LTTNGUST}"
RDEPENDS_${PN}_append_qemuarm = " ${LTTNGUST}"
RDEPENDS_${PN}_append_powerpc = " ${LTTNGUST}"
