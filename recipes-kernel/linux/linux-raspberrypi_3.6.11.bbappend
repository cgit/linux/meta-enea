FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

KERNEL_FEATURES="cfg/localversion \ 
		 cfg/with_modules \
		 cfg/embedded \
		 cfg/preempt \
		 cfg/root_nfs \
		 cfg/devtmpfs \
		 cfg/bootlogd \
		 cfg/mtd_tests \
		 cfg/ltp \
		 "

STAGING_KERNEL_FEATURES = "${KERNEL_FEATURES} cfg/debug cfg/kprobes cfg/oprofile cfg/i2c cfg/lttng cfg/perf cfg/powertop cfg/latencytop cfg/systemtap"

require recipes-kernel/linux/staging-kernel.inc
