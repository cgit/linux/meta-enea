IMAGE_FSTYPES += "ext2.gz.u-boot ext3 tar.gz"
IMAGE_FEATURES += "ptest-pkgs"
IMAGE_FEATURES += "ssh-server-dropbear"

IMAGE_INSTALL = " \
    packagegroup-enea-core-boot \
    packagegroup-enea-ptest \
    packagegroup-enea-sys \
    dropbear \
    "

inherit core-image
inherit image_types_uboot
