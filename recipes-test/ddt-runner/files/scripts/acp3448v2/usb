#!/bin/sh
#
# This script is used to test usb functionality for acp3448v2.
#

result=0
devpath=""
usbinfo=""

usbutils_is_installed=`which lsusb`
if [ -z $usbutils_is_installed ]; then
        echo "FAIL: Cannot find lsusb"
        exit 1
fi

usbdev_num=`lsusb | grep -v root\ hub | wc -l`
if [ $usbdev_num -eq 0 ]; then
        echo "FAIL: USB device is not connected"
        exit 1
else
        echo "$usbdev_num USB device(s) connected"
fi

sd=`ls -l /dev/sd[^0-9] | awk '{print $5 $6 "," $10}'`
if [ -z "$sd" ]; then
        echo "FAIL: find sd device"
        exit 1
else
        echo "PASS: find sd device"
fi

HDPARM=`which hdparm`
if [ -z $HDPARM ]; then
        result=$?
        echo "FAIL: find hdparm"
fi
        echo "PASS: find hdparm"

for s in $sd
do
    devpath=`echo "$s" | awk -F "," '{print "/sys/dev/block/" $1 ":" $2}'`
    usbinfo=`ls -l $devpath | grep usb`

    if [ -z "$usbinfo" ] ; then
        continue
    fi

    s=`echo "$s" | awk -F "," '{print $3}'`

    echo "Testing $s"
    $HDPARM -I $s
    if  [ $? -ne 0 ]; then
        result=$?
        echo "FAIL: $HDPARM -I $s Detailed/current information directly from $s"
    else
        echo "PASS: $HDPARM -I $s Detailed/current information directly from $s"
    fi

    $HDPARM -tT $s
    if  [ $? -ne 0 ]; then
        result=$?
        echo "FAIL: $HDPARM -tT $s Perform device/cache read timings on $s"
    else
        echo "PASS: $HDPARM -tT $s Perform device/cache read timings on $s"
    fi

    mkdir -p /mnt/usb_tmp
    for partition in `ls "$s"[1-9]`
    do
       echo "Testing $partition"

       mount "$partition" /mnt/usb_tmp
       if [ $? -ne 0 ]; then
          result=$?
          echo "FAIL: mount $s"
       else
          echo "PASS: mount $s"
          dd if=/dev/urandom of=/mnt/usb_tmp/writefile bs=1M count=50
          if [ $? -ne 0 ]; then
                result=$?
                echo "FAIL: write test on $s"
          else
                echo "PASS: write test on $s"
                rm -f /mnt/usb_tmp/writefile
          fi
          dd if=$s of=/mnt/usb_tmp/readfile bs=1M count=10
          if [ $? -ne 0 ]; then
                result=$?
                echo "FAIL: read test on $s"
          else
                echo "PASS: read test on $s"
                rm -f /mnt/usb_tmp/readfile
          fi
          umount /mnt/usb_tmp
      fi
    done

    rm -fr /mnt/usb_tmp
done
exit $result
