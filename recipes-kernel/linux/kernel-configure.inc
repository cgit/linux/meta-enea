
SRC_URI += "file://cfg \
            file://merge_config.sh "

configure_kernel() {
    # Add debug flavour config elements from fragments
    # Listed in increasing priority order if conflicts are detected.
    #

    #
    #combine features cfgs with defconfig
    #
    addon_features=""
	if [ -n "${2}" ]; then
        for feature in ${2}; do
            addon_features="${addon_features} ${WORKDIR}/${feature}.cfg"
        done
    fi

    O=${B} ${WORKDIR}/merge_config.sh -m ${1} ${addon_features}

    mv -f ${B}/.config ${B}/.mconfig

    #
    #in linux kernel 2.6.33, kconfig does not support alldefconfig yet.
    #
    if [ "x${PV}" = "x2.6.33" ]; then
        oe_runmake KCONFIG_ALLCONFIG=${B}/.mconfig O=${B} allnoconfig
    else
        oe_runmake KCONFIG_ALLCONFIG=${B}/.mconfig O=${B} alldefconfig
    fi
}
