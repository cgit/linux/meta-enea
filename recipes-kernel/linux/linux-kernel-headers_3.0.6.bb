PROVIDES = "linux-libc-headers"

DESCRIPTION = "linux v${PV} kernel headers"
SECTION = "devel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

PKGSUFFIX = ""
PKGSUFFIX_virtclass-nativesdk = "-nativesdk"

INHIBIT_DEFAULT_DEPS = "1"
DEPENDS += "unifdef-native"

RDEPENDS_${PN}-dev = ""
RPROVIDES_${PN} = "linux-kernel-headers"

RPROVIDES_${PN}-dev += "linux-libc-headers${PKGSUFFIX}-dev"
RPROVIDES_${PN}-dbg += "linux-libc-headers${PKGSUFFIX}-dbg"
RRECOMMENDS_${PN}-dbg = "linux-libc-headers${PKGSUFFIX}-dev (= ${EXTENDPKGV})"

require linux-3.0.6.inc

set_arch() {
	case ${TARGET_ARCH} in
		arm*)     ARCH=arm ;;
		i*86*)    ARCH=i386 ;;
		ia64*)    ARCH=ia64 ;;
		mips*)    ARCH=mips ;;
		powerpc*) ARCH=powerpc ;;
		x86_64*)  ARCH=x86_64 ;;
	esac
}

do_configure () {
        set_arch
	oe_runmake allnoconfig ARCH=$ARCH
}

do_compile () {
           :
}

do_install() {
        set_arch
        cd ${S}
        oe_runmake headers_install INSTALL_HDR_PATH=${D}${exec_prefix} ARCH=${ARCH}

        # The ..install.cmd conflicts between various configure runs
        find ${D}${includedir} -name ..install.cmd | xargs rm -f
}

BBCLASSEXTEND = "nativesdk"

# The following tasks are not required when we just want
# headers. So we override and stub them out.
do_kernel_configme() {
}

do_compile () {
}

do_validate_branches () {
}

do_kernel_configcheck () {
}
