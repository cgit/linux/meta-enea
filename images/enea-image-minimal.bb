#
# Poky specific package kits.
#

# no need for udev
VIRTUAL-RUNTIME_dev_manager = ""


#
# Enea Specific Packages
#
IMAGE_INSTALL = " \
    packagegroup-enea-core-boot \
    pramfs-init \
    "

IMAGE_LINGUAS = ""

LICENSE = "MIT"

inherit core-image
inherit image_types_uboot

IMAGE_FSTYPES += "ext2.gz.u-boot ext3 tar.gz"

# The bigger of below two will be chosen
IMAGE_ROOTFS_SIZE = "1"
# Increased the overhead factor to be able to build via nfs.
IMAGE_OVERHEAD_FACTOR = "2"
