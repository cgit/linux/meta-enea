do_prepare_config_append () {
      sed -i -e 's/# CONFIG_CHRT is not set/CONFIG_CHRT=y/' .config
      sed -i -e 's/# CONFIG_TASKSET is not set/CONFIG_TASKSET=y/' .config
      sed -i -e 's/# CONFIG_FEATURE_TASKSET_FANCY is not set/CONFIG_FEATURE_TASKSET_FANCY=y/' .config
}