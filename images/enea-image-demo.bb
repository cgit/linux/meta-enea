# Uncomment for opkg/dpkg/rpm on available on target
IMAGE_FSTYPES += "ext2.gz.u-boot ext3 tar.gz"
IMAGE_FEATURES += "package-management"

IMAGE_FEATURES += "eclipse-debug tools-debug tools-profile dbg-pkgs ssh-server-openssh"

ENEA_GPL = " \
           linx \
           linx-mod"

ENEA_GPL_qemux86 = ""
ENEA_GPL_qemuppc = ""
ENEA_GPL_armv5 = ""
ENEA_GPL_armv7a = ""

VALGRIND ?= ""
VALGRIND_powerpc ?= "valgrind"
VALGRIND_e500v2 ?= ""
VALGRIND_x86 ?= "valgrind"
VALGRIND_x86_64 ?= "valgrind"
VALGRIND_armv7a ?= "valgrind"

IMAGE_INSTALL = " \
    packagegroup-enea-core-boot \
    packagegroup-enea-debug \
    packagegroup-enea-profile \
    ${ROOTFS_PKGMANAGE_BOOTSTRAP} \
    kexec-tools \
    openssl \
    bc \
    dhcp-client \
    sqlite3 \
    pramfs-init \
    zip \
    gettext \
    gettext-runtime \
    mtd-utils \
    net-tools \
    pciutils \
    ltp \
    libuio \
    usbutils \
    lttng-tools \
    lttng-modules \
    babeltrace \
    netbase \
    sudo \
    ${ENEA_GPL} \
    ${VALGRIND} \
    "

IMAGE_INSTALL += "kernel-modules "

IMAGE_LINGUAS = ""

inherit core-image
inherit image_types_uboot

IMAGE_ROOTFS_SIZE = "1"
# Increased the overhead factor to be able to build via nfs.
IMAGE_OVERHEAD_FACTOR = "2"
