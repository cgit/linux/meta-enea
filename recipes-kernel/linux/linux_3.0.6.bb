inherit kernel
require recipes-kernel/linux/linux-dtb.inc

DESCRIPTION = "linux v${PV} kernel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

# For this kernel, the defconfig is included locally
KERNEL_DEFCONFIG="${WORKDIR}/defconfig"

KERNEL_FEATURES="cfg/localversion \
                 cfg/uio \
                "

STAGING_KERNEL_FEATURES="${KERNEL_FEATURES} cfg/debug"
STAGING_KERNEL_FEATURES_append=" cfg/kprobes cfg/oprofile cfg/lttng"
#STAGING_KERNEL_FEATURES_append_acp3448v2=""

require linux-3.0.6.inc
require staging-kernel.inc
require pramfs-3.0.inc
