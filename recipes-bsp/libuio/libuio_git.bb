SUMMARY = "libUIO"
DESCRIPTION = "Provide a generic framework for handling devices in userspace."
HOMEPAGE = "https://www.osadl.org/UIO.uio.0.html"
SECTION = "libs"
LICENSE = "GPLv2"

LIC_FILES_CHKSUM = "file://COPYING;md5=4fbd65380cdd255951079008b364516c"

SRCREV = "cc233857ba1613801e7218d07127d19608a99702"
PR = "r0"
PV = "1.0+git${SRCPV}"

SRC_URI = "git://git.linutronix.de/projects/libUIO;protocol=git \
           file://run-ptest"

DEPENDS = "virtual/libc"

S = "${WORKDIR}/git"

inherit autotools

do_install_append () {
        if [ "${PN}" = "${BPN}" -a ${PTEST_ENABLED} = "1" ]; then
            mkdir -p ${D}${PTEST_PATH}
            install -m 0755 ${WORKDIR}/run-ptest ${D}${PTEST_PATH}
        fi
}
