inherit kernel
require recipes-kernel/linux/linux-dtb.inc

DESCRIPTION = "Linux kernel headers for Freescale platforms"
SECTION = "kernel"
LICENSE = "GPLv2"

# For bsc913x kernel, the defconfig is included locally
KERNEL_DEFCONFIG="${WORKDIR}/defconfig"

KERNEL_FEATURES="cfg/localversion \
                 cfg/with_modules \
                 cfg/embedded \
                 cfg/root_nfs \
                 cfg/devtmpfs \
                 cfg/bootlogd \
                 cfg/mtd_tests \
                 "

STAGING_KERNEL_FEATURES = "${KERNEL_FEATURES} cfg/debug"
STAGING_KERNEL_FEATURES_append = " cfg/kprobes cfg/oprofile cfg/i2c"

require recipes-kernel/linux/linux-bsc913x.inc
require recipes-kernel/linux/staging-kernel.inc
require recipes-kernel/linux/pramfs-2.6.33.inc

# For linx-mod built
kernel_do_install_append() {
        cp -fR ${S}/include/generated/* ${D}/usr/src/kernel/include/generated
}
