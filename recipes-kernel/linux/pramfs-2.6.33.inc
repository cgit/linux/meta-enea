RRECOMMENDS_${PN} += "pramfs-init"

SRC_URI += "file://pramfs-2.6.33-1.1.6.tar.gz"

KERNEL_FEATURES_append = " cfg/pramfs-bsc913x"

do_apply_pramfs() {
    # Apply PRAMFS patch

    cd ${WORKDIR}/linux-2.6.33
    patch -p1 < ${WORKDIR}/pramfs-2.6.33.patch
    cd -
}

do_apply_pramfs[deptask] = "do_unpack"
addtask apply_pramfs after do_unpack before do_patch
