LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

FILESDIR = "${@os.path.dirname(bb.data.getVar('FILE',d,1))}/linux-bsc913x/${MACHINE}"

SRC_URI = "file://linux-${MACHINE}-${PV}.tar.bz2 \
           file://defconfig \
          "
SRC_URI_append = " \
                  file://0001-add-options-to-KBUILD_CFLAGS.patch \
                  file://0002-take-back-DECLARE_MUTEX-for-linx.patch \
                  file://0003-fix-nonexistant-type-u8-issue.patch \
                  file://0004-fix-wrong-WDTP_MASK-in-booke_wdt_c.patch \
                  file://0007-perf-makefile.patch \
                  file://0008-solve-bsc9131-MExtUtils-error.patch \
                 "
 
PV = "2.6.33"
PR = "r0"

S = "${WORKDIR}/linux-${PV}"

COMPATIBLE_MACHINE = "(bsc9131rdb)"
