
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"
INHIBIT_DEFAULT_DEPS = "1"

SRC_URI = "file://systest-runner \
           file://tests/ltp \
           file://tests/perf-network \
           file://tests/posixtest \
          "

STEST_PATH="${libdir}/${PN}"

FILES_${PN} +="${STEST_PATH}"
FILES_${PN} += "${bindir}/systest-runner"

do_install () {
    install -D ${WORKDIR}/systest-runner ${D}${bindir}/systest-runner
    install -d ${D}${STEST_PATH}/tests
    for file in ${WORKDIR}/tests/* ; do
        install $file ${D}${STEST_PATH}/tests
    done
}

do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_build[noexec] = "1"
