LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"
INHIBIT_DEFAULT_DEPS = "1"

SRC_URI = "file://ddt-runner \
           file://scripts"

FILES_${PN} += "${libdir}/${PN}"
FILES_${PN} += "${bindir}/ddt-runner"

do_install () {
    install -D ${WORKDIR}/ddt-runner ${D}${bindir}/ddt-runner
    if [ -d "${WORKDIR}/scripts/${MACHINE}" ]; then
        install -d ${D}${libdir}/${PN}/scripts
        for file in ${WORKDIR}/scripts/${MACHINE}/* ; do
            install $file ${D}${libdir}/${PN}/scripts
        done
    fi
}

do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_build[noexec] = "1"
