DEFAULTTUNE ?= "ppc476"

require conf/machine/include/powerpc/arch-powerpc.inc

AVAILTUNES += "ppc476"
TUNE_FEATURES_tune-ppc476 = "m32 fpu-hard ppc476"
TUNEVALID[ppc476] = "Enable ppc476 specific processor optimizations"
TUNE_CCARGS += "${@bb.utils.contains("TUNE_FEATURES", "ppc476", "-mcpu=476", "", d)}"
TUNE_PKGARCH_tune-ppc476 = "ppc476"

PACKAGE_EXTRA_ARCHS_tune-ppc476 = "powerpc-nf ppc476 ppc440 ppc405"
