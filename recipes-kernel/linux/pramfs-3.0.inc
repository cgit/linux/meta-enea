RRECOMMENDS_${PN} += "pramfs-init"

PRAMFS_VERSION = '1.4.2-3.10_patched'

SRC_URI += "file://pramfs-${PRAMFS_VERSION}.tar.gz"

KERNEL_FEATURES_append = " cfg/pramfs"
STAGING_KERNEL_FEATURES_append = " cfg/pramfs"

do_apply_pramfs() {
    # Apply PRAMFS patch

    # Already done by bitbake ->    
    # tar xvf ${WORKDIR}/pramfs-${PRAMFS_VERSION}.tar.gz -C ${WORKDIR}
    cd ${WORKDIR}/pramfs-${PRAMFS_VERSION}/
    ./patch-ker.sh ${S}
    cd -
}

do_apply_pramfs[deptask] = "do_unpack"
addtask apply_pramfs after do_unpack before do_patch
