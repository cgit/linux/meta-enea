FILESEXTRAPATHS_prepend := "${THISDIR}/files:"


SRC_URI += "file://add-no-error-uninitialized.patch \
           "

KERNEL_FEATURES="cfg/localversion \ 
		 cfg/with_modules \
		 cfg/embedded \
		 cfg/preempt \
		 cfg/root_nfs \
		 cfg/devtmpfs \
		 cfg/bootlogd \
		 cfg/mtd_tests \
		 cfg/latencytop \
		 cfg/ltp \
		 "

KERNEL_FEATURES_append_p4080ds="cfg/dpa"
KERNEL_FEATURES_append_p2041rdb="cfg/dpa"
KERNEL_FEATURES_append_p2020rdb="cfg/uio"

STAGING_KERNEL_FEATURES = "${KERNEL_FEATURES} cfg/debug cfg/kprobes cfg/oprofile cfg/i2c cfg/lttng cfg/powertop cfg/systemtap cfg/kgdb"
STAGING_KERNEL_FEATURES_RT_p4080ds="${STAGING_KERNEL_FEATURES}  cfg/rt"

require recipes-kernel/linux/staging-kernel.inc
require recipes-kernel/linux/pramfs-3.0.inc
