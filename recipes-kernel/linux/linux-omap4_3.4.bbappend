FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://fix_for_CVE-2013-2094.patch \
           "

KERNEL_FEATURES = " \
     cfg/localversion \
     cfg/with_modules \
     cfg/embedded \
     cfg/preempt \
     cfg/root_nfs \
     cfg/devtmpfs \
     cfg/bootlogd \
     cfg/mtd_tests \
     cfg/ltp \
    "

KERNEL_DEFCONFIG = "${WORKDIR}/defconfig"

STAGING_KERNEL_FEATURES = "${KERNEL_FEATURES} cfg/debug cfg/kprobes cfg/oprofile cfg/i2c cfg/lttngcfg/powertop cfg/latencytop cfg/systemtap cfg/kgdb"

require recipes-kernel/linux/staging-kernel.inc
