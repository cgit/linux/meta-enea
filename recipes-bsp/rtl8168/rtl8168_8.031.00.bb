SUMMARY = "RTL8111/8168 PCI Express 10/100/1000M Gigabit Ethernet"
DESCRIPTION = "http://www.realtek.com.tw/"
SECTION = "kernel/modules"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://Makefile;md5=27208bd930c8f7f55db149f659059e10"

DEPENDS = "linux-libc-headers module-init-tools"

SRC_URI = "file://r8168-${PV}.tar.bz2"

SRC_URI[md5sum] = "ec1654f02e2dad930bbeb0210ddab7e5"
SRC_URI[sha256sum] = "5c617b3c08aca18d1eb24d33f77df40020eb64fb32c8e4008265e08b7ffe5779"

S = "${WORKDIR}/r8168-${PV}/src"

inherit module

do_compile () {
        cd ${S}
        do_make_scripts
        unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
        oe_runmake KERNELDIR=${STAGING_KERNEL_DIR}     \
                   KERNEL_SRC=${STAGING_KERNEL_DIR}    \
                   KERNEL_VERSION=${KERNEL_VERSION}    \
                   CC="${KERNEL_CC}" LD="${KERNEL_LD}" \
                   AR="${KERNEL_AR}" \
                   modules
}

do_install () {
    mkdir -p ${D}/lib/modules/${KERNEL_VERSION}/kernel/drivers/net
    cp ${S}/*ko ${D}/lib/modules/${KERNEL_VERSION}/kernel/drivers/net
}

