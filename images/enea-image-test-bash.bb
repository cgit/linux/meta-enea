IMAGE_FSTYPES += "ext2.gz.u-boot ext3 tar.gz"
IMAGE_FEATURES += "ptest-pkgs"
IMAGE_FEATURES += "ssh-server-openssh"

PERF ?= "perf"
PERF_armv5 ?= ""
PERF_armv6 ?= ""
PERF_armv7a ?= ""

IMAGE_INSTALL = " \
    packagegroup-enea-core-boot \
    packagegroup-enea-ptest \
    packagegroup-enea-ddt \
    packagegroup-enea-sys \
    openssh \
    console-tools \
    ${PERF}\
    "

inherit core-image
inherit image_types_uboot
