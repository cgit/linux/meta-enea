# Fixup for broken ENEA environment with bash under /usr/bin/bash
# RPM nativesdk packaging breaks for meta-toolchain without this append.

FILES_${PN} += "${base_bindir}/* ${exec_prefix}/bin/*"

do_install_append () {
	mkdir -p ${D}/usr/bin
	cd ${D}/usr/bin && ln -sf /bin/bash bash
}

BBCLASSEXTEND = "native nativesdk"


