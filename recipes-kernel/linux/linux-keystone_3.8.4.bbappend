FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://fix_for_CVE-2013-2094.patch \
            file://disable_hw_checksum_offload.patch \
            file://set-keystone2-clock-frequency.patch \
           "

KERNEL_FEATURES = " \
     cfg/kgdb \
     cfg/lttng \
     cfg/perf \
     cfg/powertop \
     cfg/systemtap \
    "
KERNEL_DEFCONFIG = "${WORKDIR}/defconfig"

require kernel-configure.inc

do_configure_prepend() {

    configure_kernel ${KERNEL_DEFCONFIG} "${KERNEL_FEATURES}"

}

