DESCRIPTION = "Ptest package group"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
PR = "r0"

inherit packagegroup

ENEA_DAEMONS = " \
      at \
      cronie \
      "

ENEA_DATABASE = " \
      sqlite3 \
      "

ENEA_TRACE = " \
      babeltrace \
      ${VALGRIND} \
      "

VALGRIND ?= ""
VALGRIND_powerpc ?= "valgrind"
VALGRIND_e500v2 ?= ""
VALGRIND_x86 ?= "valgrind"
VALGRIND_x86_64 ?= "valgrind"
VALGRIND_armv7a ?= "valgrind"

ENEA_DEV = " \
      eglibc-locale \
      "

ENEA_DEVICE = " \
      libusb1-dbg \
      libusb-compat-dbg \
      usbutils \
      "

ENEA_FSYS = " \
      acl \
      pramfs-init \
      "

ENEA_HIGHAVA = " \
      kexec \
      kexec-tools \
      "

ENEA_LIBS = " \
      gettext \
      gettext-runtime \
      libstdc++ \
      libuio \
      zlib \
      "

ENEA_MESSAGING = " \
      dbus \
      "

ENEA_SCRIPTING = " \
      tcl \
      python \
      "

ENEA_SECURITY = " \
      openssl"

ENEA_SHELL = " \
      ncurses"

ENEA_UTILS = " \
      bc \
      bonnie++ \
      expat \
      "

ENEA_GPL ?= " \
      linx \
      linx-mod \
      "
ENEA_GPL_qemux86 = ""
ENEA_GPL_qemuppc = ""
ENEA_GPL_armv5 = ""
ENEA_GPL_armv7a = ""

RDEPENDS_${PN} = " \
      ${ENEA_DAEMONS} \
      ${ENEA_DATABASE} \
      ${ENEA_TRACE} \
      ${ENEA_DEV} \
      ${ENEA_DEVICE} \
      ${ENEA_FSYS} \
      ${ENEA_HIGHAVA} \
      ${ENEA_LIBS} \
      ${ENEA_MESSAGING} \
      ${ENEA_SCRIPTING} \
      ${ENEA_SECURITY} \
      ${ENEA_SHELL} \
      ${ENEA_UTILS} \
      ${ENEA_GPL} \
      ethtool \
      libxml2 \
      gdb \
      elfutils \
      "


RRECOMMENDS_${PN} = " \
      ptest-runner \
      "
