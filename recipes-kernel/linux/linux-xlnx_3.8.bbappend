FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://fix_for_CVE-2013-2094.patch \
           "
SRC_URI_append = "file://cfg/kgdb.cfg \
                  file://cfg/lttng.cfg \
                  file://cfg/perf.cfg \
                  file://cfg/powertop.cfg \
                  file://cfg/systemtap.cfg \
                  "

