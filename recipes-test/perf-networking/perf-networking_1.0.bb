SUMMARY = "A simple shell script to run network performance tests"
DESCRIPTION = "The perf-networking package installs the perf-networking \
shell script which runs tests using the netperf package"
SRC_URI += "file://perf-networking"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS_${PN}= "netperf"

do_install () {
    install -D ${WORKDIR}/perf-networking ${D}${bindir}/perf-networking
}

do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_build[noexec] = "1"
