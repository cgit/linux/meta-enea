# This .inc file allows building and deploying staging kernel + modules
# with defconfig + fragment cfgs

require kernel-configure.inc

STAGING_NAME ?= "RELEASE"
STAGING_KERNEL_FEATURES ?= ""
KERNEL_FEATURES ?= ""
MODULES_IMAGE_BASE_NAME = "modules-${PV}-${PR}-${MACHINE}"
EXTRA_OEMAKE_prepend_task-stagingkernel = "${PARALLEL_MAKE} "

kernel_do_configure_prepend() {

    configure_kernel "${KERNEL_DEFCONFIG}" "${STAGING_KERNEL_FEATURES}"

}

do_stagingkernel () {
    stagingkernel "${KERNEL_FEATURES}" ${STAGING_NAME}
    if [ -n "${STAGING_KERNEL_FEATURES_RT}" ]; then
        cd ${B}
        stagingkernel "${STAGING_KERNEL_FEATURES_RT}" "rt"
    fi

}

stagingkernel () {

    configure_kernel "${KERNEL_DEFCONFIG}" "${1}"

    # Copy defconfig to .config if .config does not exist. This allows
    # recipes to manage the .config themselves in do_configure_prepend().
    if [ -f "${WORKDIR}/defconfig" ] && [ ! -f "${B}/.config" ]; then
      cp "${WORKDIR}/defconfig" "${B}/.config"
    fi
    yes '' | oe_runmake oldconfig

    if [ ! -z "${INITRAMFS_IMAGE}" ]; then
      for img in cpio.gz cpio.lzo cpio.lzma cpio.xz; do
        if [ -e "${DEPLOY_DIR_IMAGE}/${INITRAMFS_IMAGE}-${MACHINE}.$img" ]; then
          cp "${DEPLOY_DIR_IMAGE}/${INITRAMFS_IMAGE}-${MACHINE}.$img" initramfs.$img
	fi
      done
    fi

    #
    # Build this alternative kernel
    #
    
    kernel_do_compile
    do_compile_kernelmodules

    #
    # First install the modules to deploy dir
    #
    unset CFLAGS CPPFLAGS CXXFLAGS LDFLAGS MACHINE
    if (grep -q -i -e '^CONFIG_MODULES=y$' .config); then
        oe_runmake DEPMOD=echo INSTALL_MOD_PATH="${D}" modules_install
    else
        bbnote "no modules to install"
    fi
    if [ -d "${D}/lib" ]; then
        tar -cvzf ${DEPLOY_DIR_IMAGE}/${MODULES_IMAGE_BASE_NAME}-${2}.tgz -C ${D} lib
    fi
	rm -fr ${D}/lib

    #
    # Dont build any dtb:s for the staging kernel
    #

    #
    # Drop the resulting images in the deploy dir
    #
    cd ${S}
    install -d ${DEPLOY_DIR_IMAGE}
    install -m 0644 ${KERNEL_OUTPUT} ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGE_BASE_NAME}-${2}.bin
    mv  ${KERNEL_OUTPUT} ${KERNEL_OUTPUT}-${2}

    #
    # Install the final config alongside the images
    #
    cd ${S}
    cp .config ${DEPLOY_DIR_IMAGE}/config-${PV}-${PR}-${MACHINE}-${2}.config
    rm -f .config

    #
    # Create symlinks
    #
    cd ${DEPLOY_DIR_IMAGE}
    rm -f ${KERNEL_IMAGE_SYMLINK_NAME}-${2}.bin
    ln -sf ${KERNEL_IMAGE_BASE_NAME}-${2}.bin ${KERNEL_IMAGE_SYMLINK_NAME}-${2}.bin

    rm -f config-${MACHINE}-${2}.config
    ln -sf config-${PV}-${PR}-${MACHINE}-${2}.config config-${MACHINE}-${2}.config
	
}

do_stagingkernel[dirs] = "${DEPLOY_DIR_IMAGE} ${B}"

do_stagingkernel[depends] += "u-boot-mkimage-native:do_populate_sysroot virtual/${TARGET_PREFIX}gcc:do_populate_sysroot virtual/${TARGET_PREFIX}gcc:do_populate_sysroot"

addtask stagingkernel after do_patch before do_configure

#
# For reference, copy .config to deploy image
#
do_deploy_append () {

	install -d ${DEPLOY_DIR_IMAGE}

    #
    # Drop the regular defconfig along side the others for consistency
    #
    cd ${S}
    cp .config ${DEPLOY_DIR_IMAGE}/config-${PV}-${PR}-${MACHINE}.config

    #
    # add symlink
    #
    cd ${DEPLOY_DIR_IMAGE}    
    rm -f config-${MACHINE}.config
    ln -s config-${PV}-${PR}-${MACHINE}.config config-${MACHINE}.config

    if [ -n "${STAGING_KERNEL_FEATURES_RT}" ]; then
    cd ${S}
    install -m 0644 ${KERNEL_OUTPUT}-rt ${DEPLOYDIR}/${KERNEL_IMAGE_BASE_NAME}-rt.bin

    #
    # Create symlinks
    #
    cd ${DEPLOYDIR}
    rm -f ${KERNEL_IMAGE_SYMLINK_NAME}-rt.bin
    ln -sf ${KERNEL_IMAGE_BASE_NAME}-rt.bin ${KERNEL_IMAGE_SYMLINK_NAME}-rt.bin
    fi

}

#
# Clean staging kernel related files in deploy dir
#
do_cleanstagingkernel () {
   
   cd ${DEPLOY_DIR_IMAGE}
   rm -f *-${STAGING_NAME}.*

   rm -f config-${PV}-${PR}-${MACHINE}.config
   rm -f config-${MACHINE}.config
   
}

LDFLAGS=""

do_cleanstagingkernel[nostamp] = "1"

addtask cleanstagingkernel after do_cleansstate

