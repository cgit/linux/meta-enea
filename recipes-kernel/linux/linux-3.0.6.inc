FILESDIR = "${@os.path.dirname(bb.data.getVar('FILE',d,1))}/linux-${PV}/${MACHINE}"

SRC_URI = "http://www.kernel.org/pub/linux/kernel/v3.0/linux-${PV}.tar.bz2 \
           file://defconfig \
           file://0001-add-lsi-acp3448v2-bsp-patches.patch \
           file://0002-Patch-to-head_44x.S-to-support-lsi-acp3448v2.patch \
           file://0003-Patch-drivers_dma_Makefile.patch \
           file://0004-change-arch_powerpc_kernel_setup_32_c.patch \
           file://0005-Patch-arch_powerpc_mm_tlb_nohash_c.patch \
           file://0006-backport-mmu_clear_feature-function.patch \
           file://0007-Enable-CONFIG_RELOCATABLE-for-ppc47x.patch \
           file://0008-change-serial-baudrate_to_115200.patch \
           file://0009-add-acp3448-PCIe-controller-driver.patch \
           file://0010-fix-typo-error-for-acp-pcie.patch \
	   file://0011-fix-compiler.h-__attribute_const__-redefined-problem.patch \
          "

PV = "3.0.6"
PR = "r0"

S = "${WORKDIR}/linux-${PV}"

COMPATIBLE_MACHINE = "acp3448v2"

